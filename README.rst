Perfect HTML
=======================

Tired of running auto indent? Tired of running shitty html parsers and formatters?  
Well hopefully this will cure your ailments.  
----

This package when installed will allow you to autoformat any .html files in a directory
specified.  No frills just pretty formatting.  This leverages BeautifulSoup 4 for the formatting
library.  I'm just writing the wrapper.